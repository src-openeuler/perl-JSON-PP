Name:         perl-JSON-PP
Epoch:        1
Version:      4.16
Release:      2
Summary:      JSON::XS compatible pure-Perl module.
License:      GPL-1.0-or-later OR Artistic-1.0-Perl
URL:          https://metacpan.org/release/JSON-PP
Source0:      https://cpan.metacpan.org/authors/id/I/IS/ISHIGAKI/JSON-PP-%{version}.tar.gz
BuildArch:    noarch

BuildRequires:perl-generators perl-interpreter perl(ExtUtils::MakeMaker) perl(Test::More) make

Requires: perl(Encode)
Requires: perl(Data::Dumper) perl(Math::BigFloat) perl(Math::BigInt) perl(Scalar::Util) >= 1.08 perl(utf8)

%description
JSON::PP is a pure perl JSON decoder/encoder, and (almost) compatible to much faster JSON::XS
written by Marc Lehmann in C. JSON::PP works as a fallback module when you use JSON module without having installed JSON::XS.

Because of this fallback feature of JSON.pm, JSON::PP tries not to be more JavaScript-friendly than JSON::XS
(i.e. not to escape extra characters such as U+2028 and U+2029, etc), in order for you not to lose such JavaScript-friendliness
silently when you use JSON.pm and install JSON::XS for speed or by accident. If you need JavaScript-friendly RFC7159-compliant
pure perl module, try JSON::Tiny, which is derived from Mojolicious web framework and is also smaller and faster than JSON::PP.

%package_help

%prep
%autosetup -n JSON-PP-%{version}  -p1


%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc Changes README
%{_bindir}/*
%{perl_vendorlib}/*

%files help
%{_mandir}/*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:4.16-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 18 2023 wangjiang <wangjiang37@h-partners.com> - 1:4.16-1
- upgrade version to 4.16

* Thu Nov 17 2022 wangjiang <wangjiang37@h-partners.com> - 1:4.12-2
- modify source package

* Wed Oct 26 2022 wangjiang <wangjiang37@h-partners.com> - 1:4.12-1
- upgrade version to 4.12

* Sat Dec 25 2021 tianwei <tianwei12@huawei.com> - 1:4.06-1
- upgrade version to 4.06

* Fri Jan 29 2021 yuanxin <yuanxin24@huawei.com> - 1:4.05-1
- upgrade version to 4.05

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.04-2
- Add requires

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.04-1
- Package init
